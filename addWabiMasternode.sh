#!/bin/bash

PEERNAME="<peername>"
COUCHDB_PASSWD="<passwd>"
SWARM_TOKEN="<swarm_token>"


# join this node to swarm
docker swarm join --token $SWARM_TOKEN master.walimai.com:2377

# create network
docker network create network_basic

#create coutchdb
docker create --name "couchdb" --network="network_basic"  \
-e COUCHDB_USER=admin \
-e COUCHDB_PASSWORD=$COUCHDB_PASSWD \
hyperledger/fabric-couchdb



#create peer
docker create --name "$PEERNAME" --network="network_basic"  \
-p 7051:7051 \
-p 7053:7053 \
--link couchdb:couchdb \
-e CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock \
-e CORE_PEER_ID=$PEERNAME \
-e CORE_LOGGING_PEER=debug \
-e CORE_CHAINCODE_LOGGING_LEVEL=DEBUG \
-e CORE_PEER_LOCALMSPID=Org1MSP \
-e CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/peer/msp \
-e CORE_PEER_ADDRESS=$PEERNAME:7051 \
-e CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=network_basic \
-e CORE_LEDGER_STATE_STATEDATABASE=CouchDB \
-e CORE_LEDGER_STATE_COUCHDBCONFIG_COUCHDBADDRESS=couchdb:5984 \
-e CORE_LEDGER_STATE_COUCHDBCONFIG_USERNAME=admin \
-e CORE_LEDGER_STATE_COUCHDBCONFIG_PASSWORD=$COUCHDB_PASSWD \
-e CORE_PEER_TLS_ENABLED=true \
-e CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/msp/peer/tls/server.key \
-e CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/msp/peer/tls/server.crt \
-e CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/msp/peer/tls/ca.crt \
-v /var/run/:/host/var/run/ \
-v /opt/hyperledger/config/crypto-config/peerOrganizations/org1.walimai.com/peers/$PEERNAME:/etc/hyperledger/msp/peer \
hyperledger/fabric-peer:x86_64-1.1.0  peer node start


sleep 10
# start docker 
docker start couchdb
docker start $PEERNAME